# --*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant::configure("2") do |config|

  # Provision
  ENV['ANSIBLE_CONFIG'] = 'ansible/ansible.cfg'

  config.vm.provider "virtualbox" do |v|
    v.memory = 4096
    v.customize ["modifyvm", :id, "--nictype1", "virtio"]
    v.customize ["modifyvm", :id, "--natsettings1", "1500,1024,1024,1024,1024"]
  end

  config.vm.define "gocd-server" do |machine|
    machine.vm.box = "gocd/gocd-demo"
    machine.vm.network "forwarded_port", guest: 8080, host: "8086"
    machine.vm.network "private_network", ip: "10.10.10.16"
    machine.vm.hostname = "gocd-server"

    machine.vm.provider "virtualbox" do |vb|
      vb.memory = "2048"
      vb.name = "gocd-server"
    end

    machine.vm.provision "ansible" do |ansible|
      ansible.playbook = "ansible/gocd.server.yml"
      ansible.host_key_checking = false
      # ansible.inventory_path = "ansible/inventory"
    end
  end

  config.vm.define "gocd-agent" do |machine|
    machine.vm.box = "ubuntu/trusty64"
    machine.vm.network "forwarded_port", guest: 8080, host: "8085"
    machine.vm.network "private_network", ip: "10.10.10.15"
    machine.vm.hostname = "gocd-agent"

    machine.vm.synced_folder "tech-jump-webapp", "/tech-jump-webapp"
    machine.vm.synced_folder "ansible/downloads", "/downloads"

    machine.vm.provider "virtualbox" do |vb|
      vb.memory = "512"
      vb.name = "gocd-agent"
    end

    machine.vm.provision "ansible" do |ansible|
      ansible.playbook = "ansible/gocd.agent.yml"
      # ansible.inventory_path = "ansible/inventory"
    end

  end

  config.vm.define "microservice" do |machine|
    machine.vm.box = "ubuntu/trusty64"
    machine.vm.network "forwarded_port", guest: 8080, host: "8087"
    machine.vm.network "private_network", ip: "10.10.10.17"
    machine.vm.hostname = "microservice"

    machine.vm.synced_folder ".", "/tech-jump"
    machine.vm.synced_folder "ansible/downloads", "/downloads"

    machine.vm.provider "virtualbox" do |vb|
      vb.memory = "1024"
      vb.name = "microservice"
    end

    machine.vm.provision "ansible" do |ansible|
      ansible.playbook = "ansible/microservice.yml"
      # ansible.inventory_path = "ansible/inventory"
    end
  end

end
