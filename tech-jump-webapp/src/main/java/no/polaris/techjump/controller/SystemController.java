package no.polaris.techjump.controller;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/")
public class SystemController {

    @RequestMapping(value = "/process", method = RequestMethod.GET)
    public ResponseEntity<?> list() throws IOException, InterruptedException {

        ProcessBuilder processBuilder = new ProcessBuilder("ps", "aux");
        Process process = processBuilder.start();

        String result = IOUtils.toString(process.getInputStream());
        process.waitFor();
        result = StringEscapeUtils.escapeHtml4(result);

        String[] lines = result.split("\n");
        String[] headers = lines[0].split("\\s+");

        ArrayList<Map<String, String>> ps = new ArrayList<>();
        for (int i = 1; i < lines.length; i++) {
            LinkedHashMap<String, String> processDetails = new LinkedHashMap<>();

            String processDetailsLine = result.split("\n")[i];

            for (int j = 0; j < headers.length; j++) {
                String[] value = processDetailsLine.split("\\s+", 2);
                System.out.print(processDetailsLine);
                processDetailsLine = value[1];
                processDetails.put(headers[j], value[0]);
            }
            ps.add(processDetails);
        }

        return ResponseEntity.ok(ps);
    }
//
//    @RequestMapping(value = "/process/{id}", method = RequestMethod.GET)
//    public ResponseEntity<?> details() throws IOException, InterruptedException {
//
//
//        return ResponseEntity.ok("");
//    }
//
//    @RequestMapping(value = "/process/{id}", method = RequestMethod.DELETE)
//    public ResponseEntity<?> kill(@PathVariable("id") String id) throws IOExce
//
//    ption,InterruptedException
//
//    {
//
//        ProcessBuilder processBuilder = new ProcessBuilder("kill", "-9", id);
//        Process process = processBuilder.start();
//
//        String result = IOUtils.toString(process.getInputStream());
//        process.waitFor();
//
//
//        System.out.print(result);
//
//        return ResponseEntity.ok("<pre>" + StringEscapeUtils.escapeHtml4(result) + "</pre>");
//    }

}
